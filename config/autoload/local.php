<?php
/**
 * Local Configuration Override
 *
 * This configuration override file is for overriding environment-specific and
 * security-sensitive configuration information. Copy this file without the
 * .dist extension at the end and populate values as needed.
 *
 * @NOTE: This file is ignored from Git by default with the .gitignore included
 * in ZendSkeletonApplication. This is a good practice, as it prevents sensitive
 * credentials from accidentally being committed into version control.
 */

 $dbparam =array(
     'database'=>'file_processing',
     'username'=>'root',
     'password'=>'mysqljeetokheloyaro',
     'hostname'=>'localhost'
 );
 return array(
 'service_manager' => array(
      'factories' => array(
         'Zend\Db\Adapter\Adapter' =>function($sm)use($dbparam){
            return new Zend\Db\Adapter\Adapter(array(
                'driver'=>'pdo',
                'dsn'=>'mysql:dbname='.$dbparam['database'].';host='.$dbparam['hostname'],
                'database'=>$dbparam['database'],
                'username'=>$dbparam['username'],
                'password'=>$dbparam['password'],
                'hostname'=>$dbparam['hostname']
                ));
         },
      ),
   ),
);
