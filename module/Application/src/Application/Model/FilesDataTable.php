<?php

namespace Application\Model;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Where;

class FilesDataTable extends AbstractTableGateway
{
    protected $table = 'files_data';

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
       // $this->resultSetPrototype->setArrayObjectPrototype(new Accounts());

        $this->initialize();
    }

    public function fetchAll()
    {
    	$resultSet = $this->select();
        return $resultSet;
    }
    
  
	
	public function saveData($data){
		$sql = new Sql($this->adapter);
		$insert = $sql->insert($this->table);
		$insert->values($data);
		$selectString = $sql->getSqlStringForSqlObject($insert);
		$results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
	}
	
	public function getFiles($status){
		$sql = new Sql($this->adapter);
		$select =    $sql->select()
								->from($this->table)
								->where(array('status'=>$status));
		$statements = $sql->prepareStatementForSqlObject($select);
		$results = $statements->execute()->current();				
        return $results;
	}
	
	public function changeStatus($status,$id){
		$sql    = new Sql( $this->adapter );
		$update = $sql->update();
		$update->table( $this->table );
		$update->set( array('status' => $status) );
		$update->where( array( 'id' => $id ) );

		$statement  = $sql->prepareStatementForSqlObject( $update );
		$results    = $statement->execute();
	}
	
	public function getData(){
		$sql = new Sql($this->adapter);
		$select =    $sql->select()
						->from($this->table);
		$statements = $sql->prepareStatementForSqlObject($select);
		$results = $statements->execute();				
        return iterator_to_array($results);
	}
    
}