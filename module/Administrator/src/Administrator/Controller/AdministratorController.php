<?php
namespace Administrator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Math\Rand;



class AdministratorController extends AbstractActionController
{
    
    protected $userTable;
    
    public function indexAction()
    {
		$this->layout('layout/admin');
        return new ViewModel();
    }
	
    /**
     * **********************************************************************************
     *                  USER MANAGEMENT SECTION
     * **********************************************************************************
     **/
    
     /**
      * User manager display all active and inactive user listing action process here
      * @users User data Object
      * @author developed by Trs Software Solutions
      * @return array
      **/
     public function usermanagerAction()
     {
        $users = $this->getUserTable()->fetchAll();
		$page =1;
	    $perPage = 15;
	    $userPaginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\Iterator($users));
	    $userPaginator->setCurrentPageNumber($page);
        $userPaginator->setItemCountPerPage($perPage);	
	    
        $this->layout('layout/admin');
        return array(
            'users'    => $userPaginator,
			'count' => count($user),
			'records_number' => $perPage,
            'roles'=>$roles,
            
        );	
     }
     
     /**
      * User manager display all active and inactive user listing pagination action process here
      * @users User data Object
      * @author developed by Trs Software Solutions
      * @return array
      **/
	 public  function userPaginatorAction()
	 {
	   
        $users = $this->getUserTable()->fetchAll();
        
        $page =  (int)$this->getRequest()->getPost('page', 0);
        $perPage = (int)$this->getRequest()->getPost('per_page', 0);
        
        $userPaginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\Iterator($users));
        $userPaginator->setCurrentPageNumber($page);
        $userPaginator->setItemCountPerPage($perPage);	
	
        $view = new ViewModel();
        $view->setTerminal(true)
           ->setTemplate('partial/user-paginator')
           ->setVariables(array(
                'users'    => $userPaginator,
			    'count' => count($records),
			    'records_number' => $perPage,
		    ));
           
        return $view;
	 }
     
     /**
      * User manager display all active and inactive user listing pagination layout action process here
      * @users User data Object
      * @author developed by Trs Software Solutions
      * @return array
      **/
	 public function userPaginatorLayoutAction()
	 {
	    $users = $this->getUserTable()->fetchAll();
       
        $page =  (int)$this->getRequest()->getPost('page', 0);
        $perPage = (int)$this->getRequest()->getPost('per_page', 0);
        
        $userPaginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\Iterator($users));
        $userPaginator->setCurrentPageNumber($page);
        $userPaginator->setItemCountPerPage($perPage);	
	   
        $view = new ViewModel();
        $view->setTerminal(true)
           ->setTemplate('partial/user-paginator-layout')
           ->setVariables(array(
                'users'    => $userPaginator,
			    'count' => count($records),
			    'records_number' => $perPage,
		    ));
           
        return $view;
	 }
     
     /**
      * View single user profile action process here
      * @id user id
      * @author developed by Trs Software Solutions
      * @return array
      **/
     public function profileAction()
     {
         $id = (int) $this->params()->fromRoute('id', 0);
        
        if (!$id &&!is_int($id)) {
            return $this->redirect()->toRoute('administrator', array(
                'action' => 'usermanager'
            ));
        }
        
        try {
          $user =  $this->getUserTable()->getUser($id); 
         }catch (\Exception $ex) {
             return $this->redirect()->toRoute('administrator', array(
                'action' => 'category'
            ));
        }
        
        $this->layout('layout/admin');
        return array('user'=>$user);
     }
     
     /**
      * Delete user Profile Action Process here
      * @user_id user id
      * @author developed by Trs Software Solutions
      * @return void
      **/
     
     public function deleteuserAction()
     {
         /**
          * @ Current Logined user Information
          **/
          $userObj = $this->is_userLogin();
          $current_user_id = $userObj->user_id;
         
         $user_id = (int) $this->params()->fromRoute('id', 0);
        
         if (!$user_id &&!is_int($user_id)) return $this->redirect()->toRoute('administrator', array('action' => 'usermanager'));
        
        
        try {
            
          if($current_user_id ==$user_id):
             
             $this->flashMessenger()->addErrorMessage('Current User Profile can not be deleted !');
             return $this->redirect()->toRoute('administrator', array('controller' => 'administrator', 'action' => 'usermanager'));
          endif;
            
          $user =  $this->getUserTable()->deleteUser($user_id);
          
          $this->flashMessenger()->addSuccessMessage('User Profile Deleted Successfully !');
          return $this->redirect()->toRoute('administrator', array('controller' => 'administrator', 'action' => 'usermanager'));
         
         }catch (\Exception $ex){
             return $this->redirect()->toRoute('administrator', array(
                'action' => 'usermanager'
            ));
        }
     }
    /**
     * Change user password forcefully action process here
     * @user_id user_id
     * @current_user_id Logined user ID
     * @author  developer by Trs Software solutions
     * @return void
     **/
     public function changePasswordAction()
     {
        //current user login id
        $current_user_id = $this->is_userLogin()->user_id;
          
        $user_id = (int) $this->params()->fromRoute('id', 0);
        $request = $this->getRequest();
        
        if (!$user_id &&!is_int($user_id)) return $this->redirect()->toRoute('administrator', array('action' => 'usermanager'));
        
        try {
            
            $userPassword = new UserPassword();
            $encyptPass = $userPassword->create($request->getPost('password'));
            
            $this->getUserTable()->changePassword($user_id,$encyptPass);
            
          
          
          if($current_user_id ==$user_id):
             //forcefully logout user
          endif;
          $this->flashMessenger()->addSuccessMessage('User password update Successfully !');
          return $this->redirect()->toRoute('administrator', array('controller' => 'administrator', 'action' => 'usermanager'));
          exit; 
         
         }catch (\Exception $ex){
             return $this->redirect()->toRoute('administrator', array(
                'action' => 'usermanager'
            ));
        }
     } 
     
     /**
     * Change user Role forcefully action process here
     * @user_id user_id
     * @current_user_id Logined user ID
     * @author  developer by Trs Software solutions
     * @return void
     **/
     public function changeRoleAction()
     {
        //current user login id
        $current_user_id = $this->is_userLogin()->user_id;
          
        $user_id = (int) $this->params()->fromRoute('id', 0);
        $request = $this->getRequest();
        
        if (!$user_id &&!is_int($user_id)) return $this->redirect()->toRoute('administrator', array('action' => 'usermanager'));
        
        try {
            
              $role_id = $request->getPost('role_id');
              if(empty($role_id) && !is_int($role_id))
              {
                  $this->flashMessenger()->addErrorMessage('Please Select Role !');
                  return $this->redirect()->toRoute('administrator', array('controller' => 'administrator', 'action' => 'usermanager'));

              }
            
             $this->getUserRoleTable()->updateRole($role_id,$user_id);
            
              if($current_user_id ==$user_id):
                 //forcefully logout user
                  $this->getResponse()->setHeaders($this->getResponse()->getHeaders()
                                     ->addHeaderLine('Location','/user/logout'));
                  $this->getResponse()->setStatusCode(302);
                  return $this->getResponse();
              endif;
               $this->flashMessenger()->addSuccessMessage('Update Role successfully!');
               return $this->redirect()->toRoute('administrator', array('controller' => 'administrator', 'action' => 'usermanager'));
         
         
         }catch (\Exception $ex){
             return $this->redirect()->toRoute('administrator', array(
                'action' => 'usermanager'
            ));
        }
     } 
    
     /**
      * Change user Status like active/deactive action process here
      * @user_id Get User Id
      * @author developed by Trs Software Solutions
      * @return void
      **/
     public function userstatusAction()
     {
        $user_id = (int) $this->params()->fromRoute('id', 0);
       
        
        if (!$user_id) {
            return $this->redirect()->toRoute('administrator', array(
                'action' => 'usermanager'
            ));
        }
         
            
        try {
            $state =$this->getUserTable()->getUser(array('user.user_id'=>$user_id),array('status'));
            $status =($state['status']==1)? 0: 1;
            $this->getUserTable()->updateStatus($user_id,$status);
            return true;
        }
        catch (\Exception $ex) {
             return $this->redirect()->toRoute('administrator', array(
                'action' => 'usermanager'
            ));
        }
      
     }
     
    
    
    
    /**
	 * GetUserTale method is used for getting the object of user Table from the service manager. 
	 * 
     * @author developed by Trs Software Solutions
	 * @return  entity object
	 * */
    private function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('Administrator\Model\userTable');
        }

        return $this->userTable;
    }
    
    
    
    
}
