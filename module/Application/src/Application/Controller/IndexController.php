<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Cache\Storage\Adapter\FilesystemIterator;


class IndexController extends AbstractActionController
{

	protected $filesDataTable;
	protected $filesResponseTable;
	
	public function testAction(){
		// die('asdasd');
		$check = $this->getFilesDataTable()->getData();
		$check1 = $this->getFilesResponseTable()->getData();
		
		print"<pre>";
		print_r($check);
		print_r($check1);
		die('end');
	}

    public function indexAction()
    {
		$request = $this->getRequest();
		if ($request->isPost()) {
			// Make certain to merge the files info!
			$post = array_merge_recursive(
				$request->getPost()->toArray(),
				$request->getFiles()->toArray()
			);
			
			$dir_path = PUBLIC_PATH."text-files";
			
			$this->makedirs($dir_path);
			
			$file_name = $post['file']['name'];
			
			$httpadapter = new \Zend\File\Transfer\Adapter\Http(); 
			if($httpadapter->isValid()) {
				$httpadapter->setDestination($dir_path);
				if($httpadapter->receive($file_name)) {
					$newfile = $httpadapter->getFileName(); 
				}
			}
			
			$this->getFilesDataTable()->saveData(array('keywords'=>$post['keywords'],'file'=>$file_name));
			

		}
        return new ViewModel();
    }
	
	
	public function processFileAction()
	{
		$dir_path = PUBLIC_PATH."text-files".DIRECTORY_SEPARATOR;		
		
		$check = $this->getFilesDataTable()->getFiles('1');
			
		if(isset($check['id'])) die('Some File Already being processed');
		
		$getFile = $this->getFilesDataTable()->getFiles('0');
		
		if(!isset($getFile['id'])) die('No File To Process');
		
		$this->getFilesDataTable()->changeStatus('1',$getFile['id']);		
		
		// spliting files into multiple files
		$file = fopen($dir_path.$getFile['file'] , "r"); 		
		
		$f = 1; 
		
		$file_path = $dir_path.str_replace('.txt','',$getFile['file']);
		$this->makedirs( $file_path );

		while(!feof($file))
		{
			$newfile = fopen($file_path . DIRECTORY_SEPARATOR . $f . '.txt','w'); // files are created inside a folder called data
			for($i = 1; $i <= 30000; $i++) // create new file after every 2000 records
			{
				$import = fgets($file);
				fwrite($newfile,$import);
				if(feof($file))
					break;
			}
			fclose($newfile);
			
			$f++; 
		}

		fclose($file);

		// counting total number of files
		$totalfiles = new \FilesystemIterator($file_path. DIRECTORY_SEPARATOR);
		$totalfiles = iterator_count($totalfiles);

		//processing file
		while($totalfiles >= 1){
			
			$fileToRead = fopen($file_path. DIRECTORY_SEPARATOR . $totalfiles . '.txt','r');
			
			$filecontent = file($file_path. DIRECTORY_SEPARATOR . $totalfiles . '.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
						
			$keywords = explode(',',$getFile['keywords']);	

			$insertVaules = array();
			
			foreach($filecontent as $url)
			{				
				// $match = $this->checkUrl($url,$keywords);
				$match = false;
				if($match){
					$insertVaules[] = "('{$getFile[id]}','{$url}','1')";
				}else{
					$insertVaules[] = "('{$getFile[id]}','{$url}','0')";
				}
			}				
				
			$this->getFilesResponseTable()->insertData(implode(',',$insertVaules));	
			
			$totalfiles--;
			
		} 
		
		$this->getFilesDataTable()->changeStatus('2',$getFile['id']);
		
		die('File Processing Complete');
	}
	
	
	public function makedirs($dirpath, $mode=0777) {
		return is_dir($dirpath) || mkdir($dirpath, $mode, true);
	}
	
	
	public function checkUrl($url,$keywords){
	
		$dom = new \DOMDocument();
		libxml_use_internal_errors(true);
		$dom->loadHTMLFile($url);
		$data = $dom->getElementsByTagName("body");
		if ( $data && 0 < $data->length ) {
			$body = strtolower($data->item(0)->textContent);
			
			foreach($keywords as $keyword){
				if (strpos($body,$keyword) !== false) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	######## Table Objects ##########
	
	public function getFilesDataTable()
    {
        if (!$this->filesDataTable) {
            $sm = $this->getServiceLocator();
            $this->filesDataTable = $sm->get('Application\Model\FilesDataTable');
        }

        return $this->filesDataTable;
    }
	
	public function getFilesResponseTable()
    {
        if (!$this->filesResponseTable) {
            $sm = $this->getServiceLocator();
            $this->filesResponseTable = $sm->get('Application\Model\FilesResponseTable');
        }

        return $this->filesResponseTable;
    }
}
