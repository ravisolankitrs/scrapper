<?php

namespace Administrator\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\AdapterInterface;


class User implements InputFilterAwareInterface
{
    public $user_id;
    public $first_name;
    public $last_name;
    public $email;
	public $mobile;
	public $password;
    public $address;
    public $address2;
    public $country;
    public $state;
    public $city;
    public $secondary_email;
    public $passtoken;
    public $passexpiry;
    public $status;
    public $steps;
 
	protected $dbAdapter;
    protected $inputFilter;
	
    public function __construct(AdapterInterface $dbAdapter) {
        $this->setAdapter($dbAdapter);
    }

    /**
     * Used by ResultSet to pass each database row to the entity
     */
    public function exchangeArray($data)
    {
        $this->user_id      = (isset($data['user_id'])) ? $data['user_id'] : null;
        $this->email        = (isset($data['email'])) ? $data['email'] : null;
        $this->sec_email   = (isset($data['sec_email'])) ? $data['sec_email'] : null;
		
        $this->first_name  = (isset($data['first_name'])) ? $data['first_name'] : null;
		$this->last_name   = (isset($data['last_name'])) ? $data['last_name'] : null;
        $this->address   = (isset($data['address'])) ? $data['address'] : null;
        $this->address2   = (isset($data['address2'])) ? $data['address2'] : null;
        
        $this->password     = (isset($data['password'])) ? $data['password'] : null;
        
		$this->mobile   = (isset($data['mobile'])) ? $data['mobile'] : null;
		
		$this->country   = (isset($data['country'])) ? $data['country'] : null;
        $this->states   = (isset($data['states'])) ? $data['states'] : null;
		$this->city   = (isset($data['city'])) ? $data['city'] : null;
		
        $this->steps       = (isset($data['steps'])) ? $data['steps'] : null;
        $this->status      = (isset($data['status'])) ? $data['status'] : null;
        $this->passtoken   = (isset($data['passtoken'])) ? $data['passtoken'] : null;
        $this->passexpiry  = (isset($data['passexpiry'])) ? $data['passexpiry'] : null;
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
        
    public function setAdapter($dbAdapter)
    {
        $this->dbAdapter =$dbAdapter;
        return  $this;
    }
    public function getDbAdapter()
    {
       return $this->dbAdapter; 
    }
	
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
         $isEmpty = \Zend\Validator\NotEmpty::IS_EMPTY;
         $invalidEmail = \Zend\Validator\EmailAddress::INVALID_FORMAT;
        
        
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

           

            $inputFilter->add($factory->createInput(array(
            'name' => 'email',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            $isEmpty => 'Email can not be empty.'
                        )
                    ),
                    'break_chain_on_failure' => true
                ),
                array(
                    'name' => 'EmailAddress',
                    'options' => array(
                        'messages' => array(
                            $invalidEmail => 'Enter Valid Email Address.'
                        )
                    )
                )
            )
        ))
        );
        
            $inputFilter->add($factory->createInput(array(
            'name' => 'password',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            $isEmpty => 'Password can not be empty.'
                        )
                    )
                )
            )
        )));
        $inputFilter->add($factory->createInput(array(
            'name' => 'remember_me',
            'required' => false,
            'filters' => array(
                array(
                    'name' => 'StripTags'
                ),
                array(
                    'name' => 'StringTrim'
                )
            ),
            'validators' => array(
               
            )
        )));

            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }

   public function getRegisterInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

           $inputFilter->add($factory->createInput(array(
            'name' => 'first_name', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        )));
         
        $inputFilter->add($factory->createInput(array(
            'name' => 'last_name', 
            'required' => false,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
         $inputFilter->add($factory->createInput(array(
            'name' => 'address', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
         $inputFilter->add($factory->createInput(array(
            'name' => 'address2', 
            'required' => false,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
        $inputFilter->add($factory->createInput(array(
            'name' => 'country', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
        $inputFilter->add($factory->createInput(array(
            'name' => 'states', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
         $inputFilter->add($factory->createInput(array(
            'name' => 'city', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
        
        /** mobile **/
        $inputFilter->add($factory->createInput(array(
            'name' => 'mobile', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
        $inputFilter->add($factory->createInput(array(
            'name' => 'cmobile', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array(
                    'name' => 'Identical',
                    'options' => array(
                        'token' => 'mobile', // name of first password field
                        'message'=>'Please Enter Correct Mobile Number'
                    ),
                ),
            ), 
        ))); 
        
        /** Password **/
        $inputFilter->add($factory->createInput(array(
            'name' => 'password', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
        $inputFilter->add($factory->createInput(array(
            'name' => 'verify_password', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array(
                    'name' => 'Identical',
                    'options' => array(
                        'token' => 'password', // name of first password field
                        'message'=>'Enter correct verfiy password'
                    ),
                ),
            ), 
        )));
     
       /** primary Email **/
        $inputFilter->add($factory->createInput(array(
                'name'     => 'email',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					 array(
						'name'    => 'Db\NoRecordExists',
						'options' => array(
							'table'     => 'user',
							'field'     => 'email',
							'adapter'   => $this->getDbAdapter(),
							'message'   => 'Email Address already Exists!',
						),
					), 
                    array(
                        'name'    => 'EmailAddress',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));
     $inputFilter->add($factory->createInput(array(
            'name' => 'confirm_email', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array(
                    'name' => 'Identical',
                    'options' => array(
                        'token' => 'email', // name of first password field
                        'message'=>'Enter correct Email Address'
                    ),
                ),
            ), 
        )));
       /** secondary Email **/      
         $inputFilter->add($factory->createInput(array(
                'name'     => 'sec_email',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					 
                ),
            )));
     $inputFilter->add($factory->createInput(array(
            'name' => 'confirm_sec_email', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array(
                    'name' => 'Identical',
                    'options' => array(
                        'token' => 'sec_email', // name of first password field
                        'message'=>'Enter correct Secondary Email Address'
                    ),
                ),
            ), 
        )));
        
        

            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
    public function getEmailInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
        
            $factory = new InputFactory();
        
        $inputFilter->add($factory->createInput(array(
                'name'     => 'email',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
					 array(
						'name'    => 'Db\RecordExists',
						'options' => array(
							'table'     => 'user',
							'field'     => 'email',
							'adapter'   => $this->getDbAdapter(),
							'message'   => 'Email Address is not Exists!',
						),
					), 
                    array(
                        'name'    => 'EmailAddress',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            )));
       
            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
    public function getresetPasswordInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
        
            $factory = new InputFactory();
        
            $inputFilter->add($factory->createInput(array(
            'name' => 'password', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
            ), 
        ))); 
        $inputFilter->add($factory->createInput(array(
            'name' => 'verify_password', 
            'required' => true,
            'filters' => array( 
                array('name' => 'StripTags'), 
                array('name' => 'StringTrim'), 
            ), 
            'validators' => array( 
                array(
                    'name' => 'Identical',
                    'options' => array(
                        'token' => 'password', // name of first password field
                        'message'=>'Enter correct verfiy password'
                    ),
                ),
            ), 
        ))); 
       
            $this->inputFilter = $inputFilter;        
        }

        return $this->inputFilter;
    }
	
    
}
