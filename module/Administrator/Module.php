<?php

namespace Administrator;

use Administrator\Model\TaxonomyTable;
use Administrator\Model\AdminMailTable;
use Administrator\Model\UserTable;

class Module
{
   
	
	public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
			   'Administrator\model\UserTable' => function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $table = new UserTable($dbAdapter);
                            return $table;
                },
                'Administrator\model\TaxonomyTable' => function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $table = new TaxonomyTable($dbAdapter);
                            return $table;
                },
                'Administrator\model\AdminMailTable' => function($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            $table = new AdminMailTable($dbAdapter);
                            return $table;
                }
               ),
        );
    }    

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}