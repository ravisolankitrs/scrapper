<?php

namespace Administrator\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Crypt\Password\Bcrypt;
use Zend\Db\Sql\Where;
use Zend\Math\Rand;
class UserTable extends AbstractTableGateway
{
    protected $table = 'user';
    protected $user_linker ='user_role';
    
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet();
        $this->resultSetPrototype->setObjectPrototype(new User($this->adapter));
        $this->initialize();
        
    }
	
    /** 
     * Add User Information in our database in registeration type as well as insert role entry in role table
     * @user :user Information object
     * @return type true/false
     * 
     * */
    public function saveUser($user)
    {
        
        $data = array(
             'first_name' => $user->first_name,
             'last_name'  => $user->last_name,
             'email'=>$user->email,
             'sec_email'=>$user->sec_email,
             'password'=>$user->password,
             'mobile'=>$user->mobile,
             'address'=>$user->address,
             'address2'=>$user->address2,
             'country'=>$user->country,
             'states'=>$user->states,
             'city'=>$user->city,
             'status'=>0,
             'steps'=>1,
         );
        
        $res =  $this->insert($data);
        $this->lastInsertValue = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue();
        
        $sql = new Sql($this->adapter);
        $select =   $sql->insert()
                        ->into($this->user_linker)
                        ->columns(array('user_id','role_id'))
                        ->values(array('user_id'=>$this->lastInsertValue,'role_id'=> $user->role));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $this->lastInsertValue;
    }
    
    /**
     * Get Single user Information
     * @where array take conditions
     * @columns array columns
     * @author developed by Trs Software Solutions
     * @return array
     **/
    public function getUser($where = array(), $columns = array())
    {
        try {
            $sql = new Sql($this->adapter);
            $select = $sql->select()->from(array(
                'user' => $this->table
            ));
            
            if (count($where) > 0) {
                $select->where($where);
            }
            
            if (count($columns) > 0) {
                $select->columns($columns);
            }
            
            $select->join(array('userRole' =>$this->user_linker), 'userRole.user_id = user.user_id', array('role_id'), 'LEFT');
            $select->join(array('role' => 'role'), 'userRole.role_id = role.rid', array('role_name'), 'LEFT');
            
            //var_dump($select->getSqlString());die;	
            $statement = $sql->prepareStatementForSqlObject($select);
            $users = $statement->execute()->current();
           
            return $users;
        } catch (\Exception $e) {
            throw new \Exception($e->getPrevious()->getMessage());
        }
    }
    
    /**
     * Change Password On backend and front end when user is logined
     * @user_id Current User Id
     * @password Encrypted User Password
     * @author developed by Trs software solutions
     * @return Object
     **/
    public function changePassword($user_id,$password)
    {
        
        $data = array('password'=>$password);
        $res =$this->update($data,array('user_id'=>$user_id));
        return $res;
    }
    /**
     * Delete User Action Process here
     * @user_id  User Id
     * @author developed by Trs software solutions
     * @return Object
     **/
    public function deleteUser($user_id)
    {
        
        $data = array('deleted'=>1,'status'=>0);
        $res =$this->update($data,array('user_id'=>$user_id));
        return $res;
    }
    
    /**
     * Change user Status like active/inactive 
     * @user_id  User Id
     * @status get Status Data
     * @author developed by Trs software solutions
     * @return Object
     **/
    public function updateStatus($user_id,$status)
    {
        $data = array('status'=>$status);
        $res =$this->update($data,array('user_id'=>$user_id));
        return $res;  
    }
    
    /**
     * Fetch All user Infromation action process here
     * @author developed by Trs software solutions
     * @return Object
     **/
    public function fetchAll()
    {
        
        $sql = new Sql($this->adapter);
        $mainSelect =   $sql->select()
                            ->from(array('user' =>$this->table))
                            ->join(array('urole'=>'user_role'), 'user.user_id = urole.user_id')
                            ->join(array('role'=>'role'),'role.rid=urole.role_id',array('role_name','status'));
        $statement = $sql->prepareStatementForSqlObject($mainSelect);
        $resultSet = new \Zend\Db\ResultSet\ResultSet();
        $resultSet->initialize($statement->execute());
        $resultSet->buffer();
       
        return $resultSet;
    }
    
     /**
     * Searh Users Records Action Process here
     * @author developed by Trs software solutions
     * @return Object
     **/
    public function searchRecords($string)
    {
        $sql = new Sql($this->adapter);
        $mainSelect =   $sql->select()
                            ->from(array('user' =>'user'))
                            ->join(array('urole'=>$this->table), 'user.user_id = urole.user_id')
                            ->join(array('role'=>'role'),'role.rid=urole.role_id');
        
        $where = new  Where();
        $where->and->nest->like('user.first_name',"%$string%")
                                 ->or->like('user.last_name',"%$string%")
                                 ->or->like('user.email',"%$string%")
                                  ->or->like('role.role_name',"%$string%");
        $mainSelect->where($where);
         
        $statement = $sql->prepareStatementForSqlObject($mainSelect);
        $resultset = $this->resultSetPrototype->initialize($statement->execute())->toArray();
        return $resultset;
    }

    public function updateToken($token,$email)
    {
        
        $data = array('password_token'=>$token,'token_expiry'=>time());
        $res =$this->update($data,array('email'=>$email));
        return $res;
    }
    public function updatePassword($password,$email)
    {
        
        $data = array('password'=>$password,'password_token'=>'','token_expiry'=>'');
        $res =$this->update($data,array('email'=>$email));
        return $res;
    }
    /**
     * Vaidate Token exist or not  in our user table in database
     * @token  rendom string
     * @return  user Email ID/False;
     * 
     * */
    public function verifyToken($token)
    {
        $res = $this->select(array('password_token'=>$token));
        return $res->current();
        
       
    }
	
	public function updateUserInfomation($user_id,Users $object)
	{
	       $data =array(
									'first_name'=>$object->first_name,
									'last_name'=>$object->last_name,
									'gender'=>$object->gender,
									'dob'=>$object->dob,
									'phone'=>$object->phone,
									'address'=>$object->address,
									'country'=>$object->country,
									'city'=>$object->city,
									'pincode'=>$object->pincode
								);
			$res = $this->update($data,array('user_id'=>$user_id));
			return $res;
	
	}
	/**
     * Update Image Attribute data of perticular User
     * 
     * @author Pawan Kumar <pkb.pawan@gmail.com >
     * @return void
     **/
     public function updateUser($data,$user_id)
     {
        return $this->update($data,array('user_id'=>$user_id));
     }
	

    
        
    public function checkEmail($email)
    {
        $results = $this->select(array('email'=>$email));
		return $results->current();
    }
   
}
